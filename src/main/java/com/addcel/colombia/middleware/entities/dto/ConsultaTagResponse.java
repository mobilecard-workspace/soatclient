package com.addcel.colombia.middleware.entities.dto;

import java.io.Serializable;
import java.util.List;

import com.addcel.colombia.middleware.entities.model.TagViaRapida;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
public class ConsultaTagResponse extends ResponseMc implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<TagViaRapida> tags;

}
