package com.addcel.colombia.middleware.entities.dto.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasePaquetesRequest implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String proceso;
	private String usuarioHost;
	private String claveHost;
	private String comercio;
	private String puntoVenta;
	private PaquetesRequest datos;

}
