package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="t_usuarios")
public class Usuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Long id;

    @Column(name = "SMS_CODE")
    private String smsCode;

    @Column(name = "id_aplicacion")
    private Long idAplicacion;

    @Column(name = "usr_login")
    private String login;

    @Column(name = "usr_pwd")
    private String pwd;

    @Column(name = "usr_fecha_nac")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;

    @Column(name = "usr_telefono")
    private String telefono;

    @Column(name = "operador")
    private String operador;

    @Column(name = "usr_fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;

    @Column(name = "usr_nombre")
    private String nombre;

    @Column(name = "usr_apellido")
    private String apellido;

    @Column(name = "usr_direccion")
    private String direccion;

    @Column(name = "usr_tdc_numero")
    private String tdcNumero;

    @Column(name = "usr_tdc_vigencia")
    private String tdcVigencia;

    @Column(name = "id_banco")
    private Long idBanco;

    @Column(name = "id_tipo_tarjeta")
    private Long idTipoTarjeta;

    @Column(name = "id_proveedor")
    private Long idProveedor;

    @Column(name = "id_usr_status")
    private Long idStatus;

    @Column(name = "cedula")
    private String cedula;

    @Column(name = "tipocedula")
    private Long tipoCedula;

    @Column(name = "recibirSMS")
    private Long recibirSms;

    @Column(name = "idpais")
    private Long idPais;

    @Column(name = "gemalto")
    private Long gemAlto;

    @Column(name = "email")
    private String email;

    @Column(name = "imei")
    private String imei;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "software")
    private String software;

    @Column(name = "modelo")
    private String modelo;

    @Column(name = "wkey")
    private String wkey;

    @Column(name = "telefono_original")
    private String telefonoOriginal;

    @Column(name = "usr_materno")
    private String materno;

    @Column(name = "usr_sexo")
    private String sexo;

    @Column(name = "usr_tel_casa")
    private String telCasa;

    @Column(name = "usr_tel_oficina")
    private String telOficina;

    @Column(name = "usr_id_estado")
    private Long idEstado;

    @Column(name = "usr_ciudad")
    private String ciudad;

    @Column(name = "usr_calle")
    private String calle;

    @Column(name = "usr_num_ext")
    private Long numExt;

    @Column(name = "usr_num_interior")
    private String numInterior;

    @Column(name = "usr_colonia")
    private String colonia;

    @Column(name = "usr_cp")
    private String cp;

    @Column(name = "usr_dom_amex")
    private String domAmex;

    @Column(name = "usr_terminos")
    private String terminos;

    @Column(name = "num_ext_STR")
    private String numExtension;

    @Column(name = "id_ingo")
    private String idIngo;

    @Column(name = "usr_nss")
    private String usrNss;

    @Column(name = "id_sender")
    private String idSender;

    @Column(name = "usr_rfc")
    private String rfc;

    @Column(name = "usr_curp")
    private String curp;

    @Column(name = "usr_email")
    private String email2;

    @Column(name = "pb_token")
    private String pbToken;

    @Column(name = "usr_sms")
    private String sms;

    @Column(name = "jumio_status")
    private Long jumioStatus;

    @Column(name = "jumio_reference")
    private String jumioReference;

    @Column(name = "usr_nacionalidad")
    private String nacionalidad;

    @Column(name = "usr_distrito")
    private String distrito;

    @Column(name = "usr_provincia")
    private String provincia;

    @Column(name = "usr_departamento")
    private String departamento;

    @Column(name = "usr_centro_laboral")
    private String centroLaboral;

    @Column(name = "usr_ocupacion")
    private String ocupacion;

    @Column(name = "usr_cargo_publico")
    private String cargoPublico;

    @Column(name = "usr_institucion")
    private String institucion;
    
    @OneToMany(mappedBy = "usuario")
	@JsonManagedReference
	private List<TagViaRapida> tags;
}
