package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="tarjetas_usuario")
public class TarjetaCredito implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "idtarjetasusuario")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_aplicacion")
    private Long idAplicacion;

    @Column(name = "idusuario")
    private Long idUsuario;

    @Column(name = "numerotarjeta")
    private String numero;

    @Column(name = "vigencia")
    private String vigencia;

    @Column(name = "estado")
    private Long estado;

    @Column(name = "idbanco")
    private Long idBanco;

    @Column(name = "idfranquicia")
    private Long idFranquicia;

    @Column(name = "idtarjetas_tipo")
    private Long idTarjetasTipo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecharegistro")
    private Date FechaRegistro;

    @Column(name = "ct")
    private String ct;

    @Column(name = "nombre_tarjeta")
    private String nombreTarjeta;

    @Column(name = "usrdomamex")
    private String usrDomAmex;

    @Column(name = "usrcpamex")
    private String usrCpAmex;

    @Column(name = "mobilecard")
    private Long mobilecard;

    @Column(name = "previvale")
    private String previvale;

    @Column(name = "address")
    private String address;

    @Column(name = "id_profile_viamericas")
    private String idProfileViamericas;

    @Column(name = "act_type")
    private String actType;

    @Column(name = "clabe")
    private String clabe;

    @Column(name = "num_cuenta")
    private String numCuenta;

    @Column(name = "card_token")
    private String cardToken;

    @Column(name = "masked_pan")
    private String maskedPan;

}
