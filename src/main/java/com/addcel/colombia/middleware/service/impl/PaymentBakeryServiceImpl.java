package com.addcel.colombia.middleware.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.colombia.middleware.entities.dto.PaymentBakeryDTO;
import com.addcel.colombia.middleware.entities.dto.request.PaymentBRequest;
import com.addcel.colombia.middleware.entities.dto.response.PaymentBResponse;
import com.addcel.colombia.middleware.repositories.feign.PaymentBakery;
import com.addcel.colombia.middleware.service.PaymentBakeryService;

@Service
public class PaymentBakeryServiceImpl implements PaymentBakeryService {
	
	@Autowired
	private PaymentBakery clientePaymentBakery;
	
	private static final Logger LOG = LoggerFactory.getLogger(PaymentBakeryServiceImpl.class);

	@Override
	public PaymentBResponse cobrarMontoPaymentBakery(PaymentBakeryDTO dto) throws Exception {
		PaymentBRequest paymentRequest = null;
		PaymentBResponse paymentResponse = null;
		try {
			LOG.info("Generando peticion Payment Bakery, {}",dto);
			paymentRequest = new PaymentBRequest();
			paymentRequest.setCargo(dto.getMonto());
			paymentRequest.setComision(dto.getComision());
			paymentRequest.setConcepto(dto.getConcepto());
			paymentRequest.setIdCard(dto.getIdTarjeta());
			paymentRequest.setIdProducto(dto.getIdProducto());
			paymentRequest.setIdProveedor(dto.getIdProveedor());
			paymentRequest.setIdUser(dto.getIdUsuario());
			paymentRequest.setImei(dto.getImei());
			LOG.info("Enviando peticion al servicio de Payment Bakery {}",paymentRequest);
			paymentResponse = clientePaymentBakery.procesaPago(paymentRequest, dto.getIdApp(), dto.getIdPais(), dto.getIdioma());
			LOG.info("Respuesta de PaymentBakery: "+paymentResponse);
			return paymentResponse;
		} catch (Exception e) {
			LOG.error("Error al generar el pago con Payment Bakery: {}",e.getMessage());
			throw new Exception("Error al generar el pago con Payment Bakery");
		}
	}

}
