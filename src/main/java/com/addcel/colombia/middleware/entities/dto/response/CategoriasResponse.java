package com.addcel.colombia.middleware.entities.dto.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoriasResponse implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigoCategoria;
	private String descripcion;

}
