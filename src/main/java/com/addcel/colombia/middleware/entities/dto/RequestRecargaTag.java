package com.addcel.colombia.middleware.entities.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestRecargaTag implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idUsuario;
	private Integer idPais;
	private Long idApp;
	private Long idTarjeta;
	private Double monto;
	private String imei;
	private String idioma;
	private String placa;
	private Long tagId;
}
