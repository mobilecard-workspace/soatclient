package com.addcel.colombia.middleware.utils;

public class AppUtils {
	
	public static final String BASEURI_PUNTORED_TRANSACTION = "http://localhost/PuntoRedTransaction";
	public static final String BASEURI_PTORED = "http://10.125.15.4:17201/serviciosIntegracionHost";
	//Uris Clientes Rest Punto Red
	public static final String BASEURI_RECARGAS = "/api/host/Recargas";
    public static final String BASEURI_CONSULTAS = "/api/host/Consultas";
    public static final String BASEURI_PAQUETES = "/api/host/Paquetes";
    
    //Uris cliente feign punto red
    public static final String BASEURI_PUNTORED_RECARGA = "/puntored/api/recarga";
    public static final String BASEURI_PUNTORED_PAQUETE = "/puntored/api/paquete";
    
    ///Uris controllers catalogos
    public static final String ACT_CATALOGO_OPERADORES_RECARGA = "{idApp}/{idPais}/{idioma}/apicatalogos/operadores/recargas";
    public static final String ACT_CATALOGO_OPERADORES_PAQUETE = "{idApp}/{idPais}/{idioma}/apicatalogos/operadores/paquetes";
    public static final String ACT_CATALOGO_CATEGORIAS = "{idApp}/{idPais}/{idioma}/apicatalogos/categorias";
    public static final String ACT_CATALOGO_PAQUETES = "{idApp}/{idPais}/{idioma}/apicatalogos/paquetes";
    
    //Uris controllers transaction
    public static final String RECARGAS = "/puntored/api/recarga";
    public static final String COMPRA_PAQUETE = "/puntored/api/paquete";
    
    //Uris controllers consultas
    public static final String CONSULTAR_SALDO = "{idApp}/{idPais}/{idioma}/api/consultas/saldo";
    public static final String ESTATUS_TRANSACCION = "{idApp}/{idPais}/{idioma}/api/consultas/transaccion";
	
	//Codigos de los procesos PuntoRed
	public static final String CODPROCESO_ESTADO_TRANSACCION = "04002";
	public static final String CODPOCESO_CONSULTA_SALDO = "04001";
	public static final String CODPROCESO_CONSULTA_PAQUETES_POR_CATEGORIA = "02003";
	public static final String CODPROCESO_CONSULTA_CATEGORIA_OPERADOR = "02002";
	public static final String CODPROCESO_CONSULTA_OPERADORES_PAQUETES = "02001";
	public static final String CODPROCESO_CONSULTA_OPERADORES_RECARGA = "01001";
	public static final String COD_PROCESO_COMPRA_PAQUETE = "02004";
	public static final String COD_PROCESO_RECARGA = "01002";
	
	//Payment Bakery Procesa Pagos
	public static final String URI_BASE_PAYMENT_BAKERY = "http://localhost/Bakery";
	public static final String URI_PAYMENT_BAKERY = "{idApp}/{idPais}/{idioma}/procesa/pago";
	
	//Id app Mobile Card
	public static final String ID_APP = "Mobile Card MX";
	public static final String ACTIVO = "T";
	
	//Cliente Via Rapida
	public static final String BASEURI_VIARAPIDA = "http://localhost/ViaRapida/api";
	public static final String CONSULTASALDO_VIA = "{idApp}/{idPais}/ESP/recargarTag";
	public static final String RECARGATAG_VIA = "{idApp}/{idPais}/ESP/recargarTag";
	public static final String CONSULTARPASOS_VIA = "{idApp}/{idPais}/ESP/consultarPasos";
	
	//Controllers Via Rapida
	public static final String RECARGA_TAG = "/viarapida/api/recarga";
	public static final String CONSULTASALDO_TAG = "/viarapida/api/saldo";
	public static final String CONSULTAPASOS_TAG = "/viarapida/api/pasos";
	public static final String SAVE_TAG = "/viarapida/api/saveTag";
	public static final String DELETE_TAG = "/viarapida/api/deleteTag";
	public static final String LIST_TAGS = "{idApp}/{idPais}/{idioma}/viarapida/api/listTags/{idUsuario}";
	public static final String DETALLE_TAG = "{idApp}/{idPais}/{idioma}/viarapida/api/detalleTag/{idTag}/{idUsuario}";
	
	
	///Cliente Soat
	public static final String BASEURI_SOAT ="http://localhost/SoatClient/api";
	public static final String CONSULTA_PLACA = "/soat/consultaPlaca";
	public static final String CALCULA_POLIZA = "/soat/calculaPoliza";
	public static final String EXPEDIR_POLIZA = "/soat/expedirPoliza";
	
	//Controllers Soat
	public static final String CTR_CALCULA_POLIZA = "/api/soat/calculaPoliza";
	public static final String CTR_EXPEDIR_POLIZA = "/api/soat/expedirPoliza";
	
	
	//Cliente Push
	public static final String PUSH_NOTIFICATION = "http://localhost/PushNotifications";
	public static final String SEND_PUSH = "/sendMessage";
	
	
	//Cliente MultiMarket
	public static final String BASEURI_MMPLACE = "http://localhost/MultiMarketPlace";
	public static final String COMPRA_PRODUCTO = "/api/v1/compraProducto";
	public static final String COMPRA_PRODUCTO_AMOUNT = "/api/v1/compraProductoAmount";
	public static final String CONSULTA_SERVICIO_MMP = "/api/v1/consultaServicio";
	public static final String GENERATE_HASH = "/api/v1/generateHash";
	public static final String PAGO_SERVICIO_MMP = "api/v1/pagoServicio";
	
	//Controllers MultiMarket
	public static final String CTRL_COMPRA = "/multimarket/api/compraProducto";
	public static final String CTR_CONSULTA_PRODUCTOS = "{idApp}/{idPais}/{idioma}/multimarket/api/catalogoProductos";
	public static final String CTRL_CONSULTA_SERVICIO = "{idApp}/{idPais}/{idioma}/multimarket/api/buscador";
	public static final String CTRL_PAGO_SERVICIO = "/multimarket/api/pagoServicio";
	public static final String CTRL_CATALOGO_CATEGORIAS = "{idApp}/{idPais}/{idioma}/multimarket/api/catalogoCategorias";
	public static final String CTRL_CATALOGO_SERVICIOS = "{idApp}/{idPais}/{idioma}/multimarket/api/catalogoServicios";
	public static final String CTRL_ESACANEA_FACTURA = "/multimarket/api/escaneaFactura";
	
	//Parametros MultiMarket
	public static final String MMP_DOCUMENT = "document";
	public static final String MMP_CELLPHONE = "cellphone";
	public static final String MMP_OFFICE = "snrOffice";
	public static final String MMP_EMAIL = "email";
	public static final String MMP_LICENSE = "snrLicense";
	public static final String QUERY_CONSULTA_SERVICIOS = "BILLSearchList";
	public static final String QUERY_HASH = "BILLData";
	public static final String QUERY_PRODUCTID = "60";
}
