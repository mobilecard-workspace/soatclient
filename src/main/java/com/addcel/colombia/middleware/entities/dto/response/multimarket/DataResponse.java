package com.addcel.colombia.middleware.entities.dto.response.multimarket;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer preBalance;
	private Integer posBalance;
	private Integer debt;
	private String serviceNumber;
	private Integer relAmount;
	private Integer amount;
	private Long id;
	private String createdAt;
	private String transactionId;
	private ProductResponse Product;
}
