package com.addcel.colombia.middleware.entities.dto.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultaFacturaRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String reference;
	private Long idConvenio;
	private Boolean barcode;
	private Long idUsuario;

}
