package com.addcel.colombia.middleware.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.service.CatalogosService;
import com.addcel.colombia.middleware.utils.AppUtils;
import com.addcel.colombia.middleware.utils.BdUtils;

@RestController
public class CatalogosController {
	
	@Autowired
	CatalogosService service;
	@Autowired 
	BdUtils bdUtils;
	
	private final static String INVALIDCOUNTRY = "Servicio valido solamente para Colombia";
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseMc> handleException(Exception e) {
        e.printStackTrace();
        int code = -1;
        String message = e.getMessage();
        return new ResponseEntity<>(new ResponseMc(code, message), HttpStatus.OK);
    }

	@GetMapping(value = AppUtils.ACT_CATALOGO_OPERADORES_RECARGA)
	public ResponseEntity<ResponseMc> actualizarOperadoresRecarga(@PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma) throws Exception{
		if(bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<ResponseMc>(service.actualizarCatalogoOperadoresRecarga(idApp, idPais, idioma), HttpStatus.OK);
	}
	
	@GetMapping(value = AppUtils.ACT_CATALOGO_OPERADORES_PAQUETE)
	public ResponseEntity<ResponseMc> actualizarOperadoresPaquetes(@PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma) throws Exception{
		if(bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<ResponseMc>(service.actualizarCatalogoOperadoresPaquetes(idApp, idPais, idioma), HttpStatus.OK);
	}
	
	@GetMapping(value = AppUtils.ACT_CATALOGO_CATEGORIAS)
	public ResponseEntity<ResponseMc> actualizarCatalogoCategorias(@PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma, @RequestParam String proveedor) throws Exception{
		if(bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<ResponseMc>(service.actualizarCatalogoCategorias(idApp, idPais, idioma, proveedor), HttpStatus.OK);
	}
	
	@GetMapping(value = AppUtils.ACT_CATALOGO_PAQUETES)
	public ResponseEntity<ResponseMc> actualizarCatalogoPaquetes(@PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma, @RequestParam String categoria) throws Exception{
		if(bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<ResponseMc>(service.actualizarCatalogoPaquetes(idApp, idPais, idioma, categoria), HttpStatus.OK);
	}
}
