package com.addcel.colombia.middleware.service;

import java.util.List;

import com.addcel.colombia.middleware.entities.dto.CatalogoMultiMarketResponse;
import com.addcel.colombia.middleware.entities.dto.FacturaMultiMarketResponse;
import com.addcel.colombia.middleware.entities.dto.MultiMarketServices;
import com.addcel.colombia.middleware.entities.dto.ServiciosMultiMarket;
import com.addcel.colombia.middleware.entities.dto.ServiciosMultiMarketResponse;
import com.addcel.colombia.middleware.entities.dto.TransactionResponse;
import com.addcel.colombia.middleware.entities.dto.request.ConsultaFacturaRequest;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.CompraProductosRequest;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.PagoServicioMmp;

public interface MultiMarketService {

	CatalogoMultiMarketResponse consultaProductos(Long idApp, Integer idPais, String idioma) throws Exception;
	TransactionResponse compraProducto(CompraProductosRequest request) throws Exception;
	MultiMarketServices consultaCategorias(Long idApp, Integer idPais, String idioma)throws Exception;
	ServiciosMultiMarketResponse consultaServicios(Long idApp, Integer idPais, String idioma, String categoria) throws Exception;
	TransactionResponse pagoServicio(PagoServicioMmp request) throws Exception;
	List<ServiciosMultiMarket> buscador(Long idApp, Integer idPais, String idioma, String palabra) throws Exception;
	FacturaMultiMarketResponse consultaFactura(ConsultaFacturaRequest request) throws Exception;
}
