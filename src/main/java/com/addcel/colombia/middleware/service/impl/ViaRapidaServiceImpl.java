package com.addcel.colombia.middleware.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.colombia.middleware.entities.dto.ConsultaTagResponse;
import com.addcel.colombia.middleware.entities.dto.DetalleTagResponse;
import com.addcel.colombia.middleware.entities.dto.RequestRecargaTag;
import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.entities.dto.SaveTagResponse;
import com.addcel.colombia.middleware.entities.dto.TransactionResponse;
import com.addcel.colombia.middleware.entities.dto.request.viarapida.ConsultaSaldoRequest;
import com.addcel.colombia.middleware.entities.dto.request.viarapida.RecargarTagRequest;
import com.addcel.colombia.middleware.entities.dto.request.viarapida.SaveTagRequest;
import com.addcel.colombia.middleware.entities.model.TagViaRapida;
import com.addcel.colombia.middleware.entities.model.Usuario;
import com.addcel.colombia.middleware.repositories.TagViaRapidaRepository;
import com.addcel.colombia.middleware.service.ViaRapidaService;
import com.addcel.colombia.middleware.utils.BdUtils;

@Service
public class ViaRapidaServiceImpl implements ViaRapidaService {
	
	/*@Autowired
	private ViaRapidaClient viaRapidaClient;
	@Autowired
	private PaymentBakery clientePaymentBakery;
	*/
	@Autowired
	private TagViaRapidaRepository viaRapidaRepo;
	@Autowired
	private BdUtils bdUtils;

	private static final Logger LOG = LoggerFactory.getLogger(ViaRapidaServiceImpl.class);
	
	@Override
	public TransactionResponse recargaTag(RequestRecargaTag request) throws Exception {
		TransactionResponse response = null;
		RecargarTagRequest viaRequest = null;
		//PaymentBResponse paymentResponse = null;
		LOG.info("Iniciando recarga de Tag");
		try {
			if(bdUtils.validarUsuario(request.getIdUsuario()) && bdUtils.obtenerDatosTarjeta(request.getIdTarjeta()) != null) {
				//paymentResponse = cobrarMontoPaymentBakery(request.getIdUsuario(), request.getIdTarjeta(), request.getMonto(), request.getImei(), "Recarga de Saldo Tag ViaRapida", request.getIdApp(), request.getIdPais(), request.getIdioma());
				//if (paymentResponse.getCode()== 0) {
					LOG.info("Enviando la petición a Via Rapida");
					TagViaRapida tagBD = viaRapidaRepo.findById(request.getTagId()).orElse(null);
					if (tagBD != null) {
						viaRequest = new RecargarTagRequest();
						//viaRequest.setAuthNumber(paymentResponse.getOrder().getTransactions().get(0).getAuthorization().toString());
						viaRequest.setAuthNumber("784ee4561");
						viaRequest.setIdUsuario(request.getIdUsuario());
						viaRequest.setMonto(request.getMonto());
						viaRequest.setPlaca(request.getPlaca());
						viaRequest.setTag(tagBD.getTagId());
						//BaseViaRapidaResponse viaRapidaResponse = viaRapidaClient.recargaTag(viaRequest, request.getIdApp(), request.getIdPais());
						response = new TransactionResponse();
						response.setCodigo(0);
						response.setMensaje("Recarga Exitosa");
						//response.setIdTransaccion(viaRapidaResponse.getIdTransaccion().toString());
						response.setIdTransaccion("567834");
					/*}else {
						LOG.error("El tag no existe en la BD");
						throw new Exception("El tag no existe en la BD");
					}*/
				}else {
					throw new Exception("Transaccion Denegada por Payment Bakery");
				}
			}
		} catch (Exception e) {
			LOG.error("Error al guardar el tag en la BD: "+e.getMessage());
			throw new Exception("Error al guardar el tag");
		}
		return response;
	}

	/*@Override
	public ConsultaSaldoResponse consultaSaldo(Long idTag, Long idUsuario) {
		ConsultaSaldoResponse response = null;
		LOG.info("Iniciando consulta de saldo para el tag "+idTag);
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}*/

	@Override
	public ResponseMc consultaPasos(Long idTag, Long idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SaveTagResponse saveTag(SaveTagRequest request) throws Exception {
		SaveTagResponse response = null;
		LOG.info("Iniciando el guardado del tag para el usuario "+request.getIdUsuario());
		try {
			TagViaRapida tag = new TagViaRapida();
			tag.setAlias(request.getAlias());
			tag.setPlaca(request.getPlaca());
			tag.setTagId(request.getTagId());
			tag.setCuenta("13584963");
			Usuario user = new Usuario();
			user.setId(request.getIdUsuario());
			tag.setUsuario(user);
			TagViaRapida tagResponse = viaRapidaRepo.save(tag);
			LOG.info("Tag guardado en la base de datos");
			response = new SaveTagResponse();
			response.setCodigo(0);
			response.setMensaje("Tag Guardado");
			response.setTag(tagResponse);
			LOG.info("Respuesta: "+response);
		} catch (Exception e) {
			LOG.error("Error al guardar el tag en la BD: "+e.getMessage());
			throw new Exception("Error al guardar el tag");
		}
		return response;
	}

	@Override
	public ResponseMc deleteTag(SaveTagRequest request) throws Exception {
		ResponseMc response = null;
		LOG.info("Iniciando eliminación del tag con id "+request.getIdTagBd());
		try {
			TagViaRapida tag = viaRapidaRepo.findByidViaTagAndUsuario_Id(request.getIdTagBd(), request.getIdUsuario());
			if (tag != null) {
				LOG.info("El tag corresponde al usuario");
				viaRapidaRepo.delete(tag);
				LOG.info("Tag eliminado");
				response = new ResponseMc(0, "Tag eliminado");
			}else {
				LOG.info("El tag no corresponde al usuario");
				throw new Exception("El tag no corresponde al usuario "+request.getIdUsuario());
			}
		} catch (Exception e) {
			LOG.error("Error al eliminar el tag de la BD "+e.getMessage());
			throw new Exception("Error al eliminar el tag");
		}
		return response;
	}

	@Override
	public ConsultaTagResponse listTagbyUser(Long idUsuario) throws Exception {
		ConsultaTagResponse response = null;
		List<TagViaRapida> tags = null;
		LOG.info("Consultando los tags correspondientes al usuario "+idUsuario);
		try {
			tags = viaRapidaRepo.findByUsuario_Id(idUsuario);
			if (tags != null) {
				LOG.info("El usuario tiene tags registrados");
				response = new ConsultaTagResponse();
				response.setCodigo(0);
				response.setMensaje("Consulta exitosa");
				response.setTags(tags);
			}else {
				LOG.info("El usuario no tiene tags registrados");
				response = new ConsultaTagResponse();
				response.setCodigo(-1);
				response.setMensaje("El usuario no tiene tags registrados");
			}
		} catch (Exception e) {
			LOG.error("Error al consultar los tags del usuario "+idUsuario);
			throw new Exception("Error al consultar los tags del usuario");
		}
		return response;
	}

	@Override
	public DetalleTagResponse detalleTag(Long idUsuario, Long tagId, Long idApp, Integer idPais) throws Exception {
		DetalleTagResponse response = null;
		LOG.info("Devolviendo datos del tag con id "+tagId);
		try {
			TagViaRapida tag = viaRapidaRepo.findById(tagId).orElse(null);
			response = new DetalleTagResponse();
			response.setCodigo(0);
			response.setMensaje("Consulta exitosa");
			//response.setCuenta(tag.getCuenta());
			response.setCuenta("856422314");
			response.setPlaca(tag.getPlaca());
			LOG.info("Consultando el saldo del tag");
			ConsultaSaldoRequest request = new ConsultaSaldoRequest();
			request.setIdUsuario(idUsuario);
			request.setPlaca(tag.getPlaca());
			request.setTag(tag.getTagId());
			//SaldoTagResponse saldoResponse = (SaldoTagResponse) viaRapidaClient.consultarSaldo(request, idApp, idPais);
			//response.setSaldo(saldoResponse.getSaldo());
			response.setSaldo(185.00);
			response.setMontos(getMontos());
		} catch (Exception e) {
		    LOG.error("Error al obtener el detalle del Tag "+e.getMessage());
		    throw new Exception("Error al obtener el detalle del Tag");
		}
		return response;
	}

	private List<Double> getMontos(){
		List<Double> montos = new ArrayList<Double>();
		montos.add(20.00);
		montos.add(50.00);
		montos.add(150.00);
		montos.add(300.00);
		montos.add(500.00);
		montos.add(1000.00);
		return montos;
	}
	
	/*private PaymentBResponse cobrarMontoPaymentBakery(long idUsuario, long idTarjeta, double monto, String imei, String concepto, Long idApp, Integer idPais, String idioma) throws Exception{
		PaymentBRequest paymentRequest = null;
		PaymentBResponse paymentResponse = null;
		try {
			LOG.info("Generando peticion Payment Bakery");
			paymentRequest = new PaymentBRequest();
			paymentRequest.setCargo(monto);
			paymentRequest.setComision(0.0);
			paymentRequest.setConcepto(concepto);
			paymentRequest.setIdCard(idTarjeta);
			paymentRequest.setIdProducto(210L);
			paymentRequest.setIdProveedor(220L);
			paymentRequest.setIdUser(idUsuario);
			paymentRequest.setImei(imei);
			LOG.info("Enviando peticion al servicio de Payment Bakery "+paymentRequest);
			paymentResponse = clientePaymentBakery.procesaPago(paymentRequest, idApp, idPais, idioma);
			LOG.info("Respuesta de PaymentBakery:_"+paymentResponse);
			return paymentResponse;
		} catch (Exception e) {
			LOG.error("Error al generar el pago con Payment Bakery: "+e.getMessage());
			throw new Exception("Error al generar el pago con Payment Bakery");
		}
	}*/
}
