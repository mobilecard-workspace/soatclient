package com.addcel.colombia.middleware.entities.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
public class DetalleTagResponse extends ResponseMc implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cuenta;
	private Double saldo;
	private String placa;
	private List<Double> montos;

}
