package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="bitacora_puntored")
public class BitacoraPuntoRed implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_bitacora_ptored")
	private Long idBitacoraPuntoRed;
	@Column(name = "id_usuario")
	private long idUsuario;
	@Column(name = "tipo_usuario")
	private String tipoUsuario;
	@Column(name = "accion")
	private String accion;
	@Column(name = "resultado")
	private String resultado;
	@Temporal(TemporalType.DATE)
    @Column(name="bit_fecha")
    private Date fecha;
	@Column(name = "servicio")
	private String servicio;
	@Column(name = "id_pais")
	private Integer idPais;
	@Column(name = "id_app")
	private Long idApp;
	@Column(name = "idioma")
	private String idioma;
	@Column(name = "id_producto")
	private Long idProducto;
	@Column(name = "id_proveedor")
	private Long idProveedor;
}
