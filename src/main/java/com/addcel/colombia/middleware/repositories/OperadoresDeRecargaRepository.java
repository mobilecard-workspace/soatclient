package com.addcel.colombia.middleware.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.addcel.colombia.middleware.entities.model.OperadoresDeRecarga;

@Repository
public interface OperadoresDeRecargaRepository extends JpaRepository<OperadoresDeRecarga, Integer> {

	@Query("SELECT r.nombreProveedor FROM OperadoresDeRecarga r WHERE r.codigoProveedor = :codigoProveedor")
	String findNombreproveedor(@Param("codigoProveedor") String codigoProveedor);
}
