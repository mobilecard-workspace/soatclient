package com.addcel.colombia.middleware.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.addcel.colombia.middleware.entities.model.MultiMarketProductos;

@Repository
public interface MultiMarketRepository extends JpaRepository<MultiMarketProductos, Long> {

	@Query("SELECT m FROM MultiMarketProductos m GROUP BY m.nombre")
	List<MultiMarketProductos> findAllGroupByName();
}
