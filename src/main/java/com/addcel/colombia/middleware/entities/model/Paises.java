package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="paises")
public class Paises implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idpaises")
	private Integer id;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "codigo_iata")
	private String codigo;
	@Column(name = "activo")
	private Integer activo;
	@Column(name = "url")
	private String url;

}
