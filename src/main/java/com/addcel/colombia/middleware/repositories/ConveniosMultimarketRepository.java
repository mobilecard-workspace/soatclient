package com.addcel.colombia.middleware.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.addcel.colombia.middleware.entities.model.ConveniosMultiMarket;

@Repository
public interface ConveniosMultimarketRepository extends JpaRepository<ConveniosMultiMarket, Long> {

	@Query("SELECT c.categoria FROM ConveniosMultiMarket c WHERE c.categoria NOT LIKE \'%OTROS%\' GROUP BY c.categoria")
	List<String> findCategorias();
	
	Long countByCategoria(String categoria);
	
	List<ConveniosMultiMarket> findByCategoria(String categoria);
	
	List<ConveniosMultiMarket> findByNombreContaining(String name);
}
