package com.addcel.colombia.middleware.repositories.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import com.addcel.colombia.middleware.entities.dto.request.multimarket.ProductQuery;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.ProductSellAmount;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.ProductsSellRequest;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.ServicePayRequest;
import com.addcel.colombia.middleware.entities.dto.response.multimarket.BillDataResponse;
import com.addcel.colombia.middleware.entities.dto.response.multimarket.BillSearchListResponse;
import com.addcel.colombia.middleware.entities.dto.response.multimarket.ProductsSellResponse;
import com.addcel.colombia.middleware.utils.AppUtils;

@FeignClient(name = "multiMarket", url = AppUtils.BASEURI_MMPLACE)
public interface MultiMarketClient {

	@PostMapping(value = AppUtils.COMPRA_PRODUCTO)
	public ProductsSellResponse comprarProducto(ProductsSellRequest request);
	
	@PostMapping(value = AppUtils.COMPRA_PRODUCTO_AMOUNT)
	public ProductsSellResponse comprarProductoAmount(ProductSellAmount request);
	
	@PostMapping(value = AppUtils.CONSULTA_SERVICIO_MMP)
	public List<BillSearchListResponse> consultarServicio(ProductQuery query);
	
	@PostMapping(value = AppUtils.GENERATE_HASH)
	public BillDataResponse consultarFactura(ProductQuery query);
	
	@PostMapping(value = AppUtils.PAGO_SERVICIO_MMP)
	public ProductsSellResponse pagarServicio(ServicePayRequest request);
}
