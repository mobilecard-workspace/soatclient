package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="config_puntored")
public class ConfigPuntoRed implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_config_ptored")
	private Integer id;
	@Column(name = "usuario_host")
	private String usuarioHost;
	@Column(name =  "clave_host")
	private String claveHost;
	@Column(name = "comercio")
	private String comercio;
	@Column(name = "punto_venta")
	private String puntoVenta;
	@Column(name = "terminal")
	private String terminal;
	@Column(name = "claveCXR")
	private String claveCxr;
}
