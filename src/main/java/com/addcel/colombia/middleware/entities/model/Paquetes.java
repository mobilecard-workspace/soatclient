package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="paquetes_puntored")
public class Paquetes implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_paquete_ptored")
	private Integer id;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "codigo_paquete")
	private String codigoPaquete;
	@Column(name = "valor")
	private String valor;
	@Column(name = "sku")
	private String sku;
	@Column(name = "activo")
	private int activo;
	@ManyToOne
	@JoinColumn(name = "id_categoria_ptored", referencedColumnName = "id_categoria_ptored", nullable = false)
	@JsonBackReference
	private Categorias categoria;
}
