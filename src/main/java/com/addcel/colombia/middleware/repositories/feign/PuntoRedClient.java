package com.addcel.colombia.middleware.repositories.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;

import com.addcel.colombia.middleware.entities.dto.request.BaseCategoriasRequest;
import com.addcel.colombia.middleware.entities.dto.request.BasePaquetesRequest;
import com.addcel.colombia.middleware.entities.dto.request.BaseTransaccionRequest;
import com.addcel.colombia.middleware.entities.dto.request.OperadoresRecargaRequest;
import com.addcel.colombia.middleware.entities.dto.response.BaseCategoriasResponse;
import com.addcel.colombia.middleware.entities.dto.response.BaseConsultaPaquetesResponse;
import com.addcel.colombia.middleware.entities.dto.response.BaseOperadoresResponse;
import com.addcel.colombia.middleware.entities.dto.response.BaseSaldoResponse;
import com.addcel.colombia.middleware.entities.dto.response.BaseTransaccionResponse;
import com.addcel.colombia.middleware.utils.AppUtils;


@FeignClient(name = "puntoRed", url =AppUtils.BASEURI_PTORED)
public interface PuntoRedClient {
	
	@PostMapping(value = AppUtils.BASEURI_RECARGAS, consumes=MediaType.APPLICATION_JSON_VALUE)
	BaseOperadoresResponse getoperadoresRecargas(OperadoresRecargaRequest request);

	@PostMapping(value = AppUtils.BASEURI_CONSULTAS, consumes=MediaType.APPLICATION_JSON_VALUE)
	BaseSaldoResponse consultaDeSaldo(OperadoresRecargaRequest request);
	
	@PostMapping(value = AppUtils.BASEURI_CONSULTAS, consumes=MediaType.APPLICATION_JSON_VALUE)
	BaseTransaccionResponse consultaEstadoTransaccion(BaseTransaccionRequest request);
	
	@PostMapping(value = AppUtils.BASEURI_PAQUETES, consumes=MediaType.APPLICATION_JSON_VALUE)
	BaseOperadoresResponse consultaOperadoresPaquetes(OperadoresRecargaRequest request);
	
	@PostMapping(value = AppUtils.BASEURI_PAQUETES, consumes=MediaType.APPLICATION_JSON_VALUE)
	BaseCategoriasResponse consultaCategoriasOperador(BaseCategoriasRequest request);
	
	@PostMapping(value = AppUtils.BASEURI_PAQUETES, consumes=MediaType.APPLICATION_JSON_VALUE)
	BaseConsultaPaquetesResponse consultaPaquetesPorCategoria(BasePaquetesRequest request);
	
}
