package com.addcel.colombia.middleware;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import com.addcel.colombia.middleware.entities.dto.AppUser;
import com.addcel.colombia.middleware.entities.model.AppAuthorization;
import com.addcel.colombia.middleware.repositories.AppAuthorizationRepository;
import com.addcel.colombia.middleware.utils.AppUtils;

@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class ColombiaMiddlewareApplication extends SpringBootServletInitializer{
	
	@Autowired
	private AppAuthorizationRepository authRepo;
	
	private static final Logger LOG = LoggerFactory.getLogger(ColombiaMiddlewareApplication.class);
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder app) {
		return app.sources(ColombiaMiddlewareApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(ColombiaMiddlewareApplication.class, args);
	}
	
	@Bean
	@PostConstruct
	public AppUser appUser() {
		LOG.info("Obteniendo los datos de autenticacion para Mobile Card");
		AppUser appUser = new AppUser();
		try {
			AppAuthorization auth = authRepo.findByIdApplicationAndActivo(AppUtils.ID_APP, AppUtils.ACTIVO);
			if (auth != null) {
				appUser.setUsername(auth.getUsername());
				appUser.setPassword(auth.getPassword());
			}else {
				LOG.info("No es posible obtener los datos de autenticacion para Mobile Card");
			}
		} catch (Exception e) {
			LOG.info("Error al obtener la autenticacion desde BD "+e.getMessage());
		}
		return appUser;
	}

}
