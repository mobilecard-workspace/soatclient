package com.addcel.colombia.middleware.entities.dto.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaqueteRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String numero;
	private String valor;
	private String terminal;
	private String claveCXR;
	private String codigoProveedor;
	private String trace;
	private String codigoCategoria;
	private String codigoPaquete;
	private String sku;
}
