package com.addcel.colombia.middleware.entities.dto.request.soat;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultaPolizaRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer clase;
	private Integer cilindraje;
	private Double capacidad;
	private Integer modelo;
	private Integer pasajeros;
	private String placa;
}
