package com.addcel.colombia.middleware.service;

import com.addcel.colombia.middleware.entities.dto.ConsultaTagResponse;
import com.addcel.colombia.middleware.entities.dto.DetalleTagResponse;
import com.addcel.colombia.middleware.entities.dto.RequestRecargaTag;
import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.entities.dto.SaveTagResponse;
import com.addcel.colombia.middleware.entities.dto.TransactionResponse;
import com.addcel.colombia.middleware.entities.dto.request.viarapida.SaveTagRequest;

public interface ViaRapidaService {
	TransactionResponse recargaTag(RequestRecargaTag request) throws Exception;
	//ConsultaSaldoResponse consultaSaldo(Long idTag, Long idUsuario);
	ResponseMc consultaPasos(Long idTag, Long idUsuario);
	SaveTagResponse saveTag(SaveTagRequest request) throws Exception;
	ResponseMc deleteTag(SaveTagRequest request) throws Exception;
	ConsultaTagResponse listTagbyUser(Long idUsuario) throws Exception;
	DetalleTagResponse detalleTag(Long idUsuario, Long tagId, Long idApp, Integer idPais) throws Exception;
}
