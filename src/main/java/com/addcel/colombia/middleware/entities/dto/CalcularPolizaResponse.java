package com.addcel.colombia.middleware.entities.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
public class CalcularPolizaResponse extends ResponseMc implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nombre;
	private String cc;
	private String telefono;
	private String email;
	private String marca;
	private String linea;
	private String modelo;
	private String placa;
	private String categoria;
	private Double total;
	private String vigenciaIni;
	private String vigenciaFin;
	private Double seguroSoat;

}
