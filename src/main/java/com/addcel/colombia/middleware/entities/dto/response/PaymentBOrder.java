package com.addcel.colombia.middleware.entities.dto.response;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentBOrder {

	private Double amount;
	private Boolean antifraud;
	private Date created;
	private String additional;
	private String ip;
	private String uuid;
	private Date created_from_client_timezone;
	private String token;
	private String service;
	private Boolean paid;
	private Boolean safe;
	private String currency;
	private String refunded;
	private Long customer;
	private List<PaymentTransactions> transactions;
}
