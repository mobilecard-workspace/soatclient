package com.addcel.colombia.middleware.entities.dto.request.multimarket;

import java.io.Serializable;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductSellAmount implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer amount;
	private String productId;
	private Map<String, String> data;
}
