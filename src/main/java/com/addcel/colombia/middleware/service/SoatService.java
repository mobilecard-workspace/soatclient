package com.addcel.colombia.middleware.service;

import com.addcel.colombia.middleware.entities.dto.CalculaPolizaRequest;
import com.addcel.colombia.middleware.entities.dto.CalcularPolizaResponse;
import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.entities.dto.request.soat.ExpedirPolizaRequest;

public interface SoatService {

	CalcularPolizaResponse calcularPoliza(CalculaPolizaRequest request)throws Exception;
	ResponseMc expedirPoliza(ExpedirPolizaRequest request)throws Exception;
}
