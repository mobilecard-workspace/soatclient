package com.addcel.colombia.middleware.entities.dto.request.push;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PushRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long modulo;
	private long id_usuario;
	private String tipoUsuario;
	private String idioma;
	private int idPais;
	private int idApp;
	private List<PushParams> params;
}
