package com.addcel.colombia.middleware.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.addcel.colombia.middleware.entities.model.SoatTransaction;

public interface SoatTransactionRepository extends JpaRepository<SoatTransaction, Long> {

	SoatTransaction findByPlacaAndIdUsuarioAndStatus(String placa, Long idUsuario, Integer status);
}
