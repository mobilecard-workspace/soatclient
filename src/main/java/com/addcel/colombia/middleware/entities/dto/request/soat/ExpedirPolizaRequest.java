package com.addcel.colombia.middleware.entities.dto.request.soat;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpedirPolizaRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String placa;
	private Long idUsuario;
	private Integer idPais;
	private Long idApp;
	private String idioma;
	private Long idTarjeta;

}
