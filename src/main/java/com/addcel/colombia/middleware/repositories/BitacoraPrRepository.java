package com.addcel.colombia.middleware.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.addcel.colombia.middleware.entities.model.BitacoraPuntoRed;

@Repository
public interface BitacoraPrRepository extends JpaRepository<BitacoraPuntoRed, Long> {
	
}
