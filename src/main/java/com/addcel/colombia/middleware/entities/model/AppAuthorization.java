package com.addcel.colombia.middleware.entities.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "APP_AUTHORIZATION")
public class AppAuthorization {

	@Id
    @Column(name = "id_app_auth")
    private Integer idAppAuth;
    @Column(name = "id_application")
    private String idApplication;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "activo")
    private String activo;
}
