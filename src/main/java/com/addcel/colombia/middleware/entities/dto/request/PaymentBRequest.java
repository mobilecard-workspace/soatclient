package com.addcel.colombia.middleware.entities.dto.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentBRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idUser;
	private Long idCard;
	private String cargo;
	private String comision;
	private String concepto;
	private String referencia;
	private Long idProveedor;
	private Long idProducto;
	private String imei;
	private String modelo;
	private Integer software;
	private float lat;
	private float lon;
	private Long establecimientoId;
}
