package com.addcel.colombia.middleware.entities.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
public class ConsultaSaldoResponse extends ResponseMc implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double saldo;

}
