package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="viarapida_tags")
public class TagViaRapida implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_viarapida_tags")
	private Long idViaTag;
	@Column(name = "tag_id")
	private String tagId;
	@Column(name = "placa")
	private String placa;
	@Column(name = "alias")
	private String alias;
	@ManyToOne
	@JoinColumn(name = "fk_usuario", referencedColumnName = "id_usuario", nullable = false)
	@JsonBackReference
	private Usuario usuario;
	@Column(name = "cuenta")
	private String cuenta;
}
