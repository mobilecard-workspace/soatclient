package com.addcel.colombia.middleware.entities.dto.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseTransaccionResponse implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean estado;
	private String mensaje;
	private String codigo;
	private EstadoTransaccionResponse datos;
}
