package com.addcel.colombia.middleware.entities.dto.response.soat;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultarInfoVehiculoResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idConsulta;
	private String snConsultaRuntExitosa;
	private String observacionesServicio;
	private Integer idTipoServicio;
	private String tipoServicio;
	private Integer idClaseVehiculo;
	private String claseVehiculo;
	private Integer idMarca;
	private String marca;
	private Integer idLinea;
	private String linea;
	private Integer aaaa_modelo;
	private Integer idColor;
	private String color;
	private String noMotor;
	private String noChasis;
	private String noVin;
	private Integer cnt_cc;
	private String divipola;
	private Double cnt_toneladas;
	private Integer pesoBrutoVehicular;
	private Integer cnt_ocupantes;
	private String codMarcaSise;
	private Integer codLineaSise;
	private Integer codClaseSise;
	private Integer cod_destino;
	private Integer idTipoCarroceria;
	private String tipoCarroceria;
	private Integer idTipoCombustible;
	private String tipoCombustible;
	private String estadoDelVehiculo;
	private String organismoTransito;
	private List<Homologaciones> homologaciones;
}
