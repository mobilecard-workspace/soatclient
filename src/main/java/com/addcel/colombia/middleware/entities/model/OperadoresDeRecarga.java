package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="operadores_recarga_puntored")
public class OperadoresDeRecarga implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_operador_ptored")
	private Integer id;
	@Column(name = "codigo_proveedor")
	private String codigoProveedor;
	@Column(name = "nombre_proveedor")
	private String nombreProveedor;
	@Column(name = "activo")
	private int activo;
	
}
