package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="multi_market_productos")
public class MultiMarketProductos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_producto")
	private Long idProducto;
	@Column(name = "id_mmp")
	private Integer idMmp;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "valor")
	private String valor;
	@Column(name = "amount")
	private Boolean amount;
	@Column(name = "params")
	private String params;

}
