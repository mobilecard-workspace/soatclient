package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="categorias_puntored")
public class Categorias implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_categoria_ptored")
	private Integer id;
	@Column(name = "codigo_categoria")
	private String codigoCategoria;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "activo")
	private Integer activo;
	@ManyToOne
	@JoinColumn(name = "id_operador_ptored", referencedColumnName = "id_operador_ptored", nullable = false)
	@JsonBackReference
	private OperadoresDePaquetes proveedor;
	@OneToMany(mappedBy = "categoria")
	@JsonManagedReference
	private List<Paquetes> paquetes;
}
