package com.addcel.colombia.middleware.service.impl;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.colombia.middleware.entities.dto.CalculaPolizaRequest;
import com.addcel.colombia.middleware.entities.dto.CalcularPolizaResponse;
import com.addcel.colombia.middleware.entities.dto.PaymentBakeryDTO;
import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.entities.dto.request.soat.ConsultaPlacaRequest;
import com.addcel.colombia.middleware.entities.dto.request.soat.ConsultaPolizaRequest;
import com.addcel.colombia.middleware.entities.dto.request.soat.ExpedirPolizaRequest;
import com.addcel.colombia.middleware.entities.dto.request.soat.RequestExpedirPoliza;
import com.addcel.colombia.middleware.entities.dto.response.PaymentBResponse;
import com.addcel.colombia.middleware.entities.dto.response.soat.CalculaPolizaResponse;
import com.addcel.colombia.middleware.entities.dto.response.soat.ConsultarInfoVehiculoResponse;
import com.addcel.colombia.middleware.entities.model.SoatTransaction;
import com.addcel.colombia.middleware.entities.model.Usuario;
import com.addcel.colombia.middleware.repositories.SoatTransactionRepository;
import com.addcel.colombia.middleware.repositories.feign.SoatClient;
import com.addcel.colombia.middleware.service.PaymentBakeryService;
import com.addcel.colombia.middleware.service.PushService;
import com.addcel.colombia.middleware.service.SoatService;
import com.addcel.colombia.middleware.utils.BdUtils;

@Service
public class SoatServiceImpl implements SoatService {
	
	@Autowired
	private BdUtils bdUtils;
	@Autowired
	private SoatClient client;
	@Autowired
	private SoatTransactionRepository soatRepo;
	@Autowired
	private PushService pushService;
	@Autowired
	private PaymentBakeryService paymentService;

	private static final Logger LOG = LoggerFactory.getLogger(SoatServiceImpl.class);
	
	@Override
	public CalcularPolizaResponse calcularPoliza(CalculaPolizaRequest request) throws Exception {
		CalcularPolizaResponse response = null;
		Usuario user = null;
		CalculaPolizaResponse polizaResponse = null;
		ConsultarInfoVehiculoResponse vehiculoResponse = null;
		SoatTransaction soatTransaction = null;
		try {
			LOG.info("Iniciando el proceso para calcular la poliza, request:{}",request);
			LOG.info("Validando al usuario en BD");
			if (bdUtils.validarUsuario(request.getIdUsuario())) {
				soatTransaction = bdUtils.consultarPolizaBd(request.getPlaca(), request.getIdUsuario(), 0);
				///Armamos la respuesta del servicio que se moistrará en el front
				user = bdUtils.consultarUsuario(request.getIdUsuario());
				if (soatTransaction == null) {
					soatTransaction = new SoatTransaction();
					LOG.info("Consultando la placa en SOAT");
					vehiculoResponse = consultarPlacaSoat(request.getPlaca());
					polizaResponse = calcularPolizaSoat(vehiculoResponse.getCnt_toneladas(),vehiculoResponse.getCnt_cc(),vehiculoResponse.getHomologaciones().get(0).getClase().getCodClase(),vehiculoResponse.getAaaa_modelo(),vehiculoResponse.getCnt_ocupantes(),request.getPlaca());
					
					soatTransaction.setAnoModelo(vehiculoResponse.getAaaa_modelo());
					soatTransaction.setClaseSoat(Integer.toString(vehiculoResponse.getHomologaciones().get(0).getClase().getCodClase()));
					soatTransaction.setCnt_ocupantes(vehiculoResponse.getCnt_ocupantes());
					soatTransaction.setCnt_toneladas(vehiculoResponse.getCnt_toneladas().intValue());
					soatTransaction.setCntCc(vehiculoResponse.getCnt_cc());
					soatTransaction.setCodMarca(vehiculoResponse.getMarca());
					soatTransaction.setCodMarcaSise(vehiculoResponse.getCodMarcaSise());
					soatTransaction.setCodTipoVehMinTrans(vehiculoResponse.getHomologaciones().get(0).getClase().getCodTipoVehMinTrans());
					soatTransaction.setFechaExpedicion(polizaResponse.getFechaExpedicion());
					soatTransaction.setFechaFinVigencia(polizaResponse.getFechaFinVigencia());
					soatTransaction.setFechaInicioVigencia(polizaResponse.getFechaInicioVigencia());
					soatTransaction.setIdUsuario(request.getIdUsuario());
					soatTransaction.setLinea(vehiculoResponse.getLinea());
					soatTransaction.setModelo(Integer.toString(vehiculoResponse.getCodLineaSise()));
					soatTransaction.setNoChasis(vehiculoResponse.getNoChasis());
					soatTransaction.setNoMotor(vehiculoResponse.getNoMotor());
					soatTransaction.setNoVin(vehiculoResponse.getNoVin());
					soatTransaction.setPlaca(request.getPlaca());
					soatTransaction.setTarifa(polizaResponse.getTarifa());
					soatTransaction.setValorContribucion(polizaResponse.getValorContribucion());
					soatTransaction.setValorPrima(polizaResponse.getValorPrima());
					soatTransaction.setValorTasaRunt(polizaResponse.getValorTasaRUNT());
					soatTransaction.setValorTotalPagar(polizaResponse.getValorTotalPagar());
					soatTransaction.setValorTotalPoliza(polizaResponse.getValorTotalPoliza());
					soatTransaction.setCodDestino(vehiculoResponse.getHomologaciones().get(0).getCodDestinoSise());
					soatTransaction.setStatus(0);
					LOG.info("Guardando los datos de la poliza en la BD {}",soatTransaction);
					soatRepo.save(soatTransaction);
					response = new CalcularPolizaResponse();
					response.setCategoria(vehiculoResponse.getTipoServicio());
					response.setCc(user.getCedula());
					response.setCodigo(0);
					response.setEmail(user.getEmail());
					response.setLinea(vehiculoResponse.getLinea());
					response.setMarca(vehiculoResponse.getMarca());
					response.setMensaje("Póliza calculada con éxito");
					response.setModelo(Integer.toString(vehiculoResponse.getAaaa_modelo()));
					response.setNombre(user.getNombre()+" "+user.getApellido());
					response.setPlaca(request.getPlaca());
					response.setSeguroSoat(polizaResponse.getValorTotalPoliza().doubleValue());
					response.setTelefono(user.getTelefono());
					response.setTotal(polizaResponse.getValorTotalPagar().doubleValue());
					response.setVigenciaFin(polizaResponse.getFechaFinVigencia());
					response.setVigenciaIni(polizaResponse.getFechaInicioVigencia());
					
				} else {
					LOG.info("La placa ya habia sido calculada previamente");
					response = new CalcularPolizaResponse();
					response.setCategoria(soatTransaction.getClaseSoat());
					response.setCc(user.getCedula());
					response.setCodigo(0);
					response.setEmail(user.getEmail());
					response.setLinea(soatTransaction.getLinea());
					response.setMarca(soatTransaction.getCodMarca());
					response.setMensaje("Póliza calculada con éxito");
					response.setModelo(Integer.toString(soatTransaction.getAnoModelo()));
					response.setNombre(user.getNombre()+" "+user.getApellido());
					response.setPlaca(request.getPlaca());
					response.setSeguroSoat(Double.valueOf(soatTransaction.getValorTotalPoliza()));
					response.setTelefono(user.getTelefono());
					response.setTotal(Double.valueOf(soatTransaction.getValorTotalPagar()));
					response.setVigenciaFin(soatTransaction.getFechaFinVigencia());
					response.setVigenciaIni(soatTransaction.getFechaInicioVigencia());
				}
			}else {
				LOG.error("El usuario no existe en la BD");
				throw new Exception("Usuario inválido");
			}
		} catch (Exception e) {
			LOG.error("Error al calcular la poliza {}", e.getMessage());
			throw new Exception(e.getMessage());
		}
		LOG.info("Respuesta: {}", response);
		return response;
	}


	@Override
	public ResponseMc expedirPoliza(ExpedirPolizaRequest request) throws Exception {
		ResponseMc response = null;
		PaymentBResponse paymentResponse = null;
		SoatTransaction soatTransaction = null;
		Usuario user = null;
		RequestExpedirPoliza expedirPolizaRequest = null;
		try {
			LOG.info("Inicializando servicio de expedicion de poliza, request: {}",request);
			LOG.info("Validando al usuario y la tarjeta en la Base de Datos");
			if(bdUtils.validarUsuario(request.getIdUsuario()) && bdUtils.obtenerDatosTarjeta(request.getIdTarjeta()) != null) {
				LOG.info("obteniendo los datos del usario y el calculo de su poliza en BD");
				user = bdUtils.consultarUsuario(request.getIdUsuario());
				soatTransaction = bdUtils.consultarPolizaBd(request.getPlaca(), request.getIdUsuario(), 0);
				LOG.info("Datos de la poliza "+soatTransaction);
				
				if (soatTransaction.getStatus() == 0) {
					LOG.info("Cobrando el monto con Payment Bakery");
					Integer montoString = soatTransaction.getValorTotalPoliza();
					//paymentResponse = cobrarMontoPaymentBakery(request.getIdUsuario(), request.getIdTarjeta(), soatTransaction.getValorTotalPoliza(), "", "Poliza de Seguro SOAT "+request.getPlaca(), request.getIdApp(), request.getIdPais(), request.getIdioma());
					paymentResponse = paymentService.cobrarMontoPaymentBakery(new PaymentBakeryDTO(request.getIdUsuario(), request.getIdTarjeta(), Integer.toString(montoString*100), "", "Expedicion poliza SOAT", request.getIdApp(), request.getIdPais(), request.getIdioma(), 210L, 2100L, "0.00"));
					LOG.info("Respuesta de Payment Bakery: "+paymentResponse);
					
					if (paymentResponse.getCode() != -3100 && !paymentResponse.getOrder().getTransactions().get(0).getAuthorization().equals("NONE")){
						LOG.info("Pago exitoso en payment, procediendo a expedir la poliza en SOAT");
						expedirPolizaRequest = new RequestExpedirPoliza();
						expedirPolizaRequest.setAnoModelo(soatTransaction.getAnoModelo());
						expedirPolizaRequest.setApellido(user.getApellido());
						expedirPolizaRequest.setClaseSoat(soatTransaction.getClaseSoat());
						expedirPolizaRequest.setCnt_ocupantes(soatTransaction.getCnt_ocupantes());
						expedirPolizaRequest.setCnt_toneladas(soatTransaction.getCnt_toneladas());
						expedirPolizaRequest.setCntCc(soatTransaction.getCntCc());
						expedirPolizaRequest.setCodMarca(soatTransaction.getCodMarca());
						expedirPolizaRequest.setCodMarcaSise(soatTransaction.getCodMarcaSise());
						expedirPolizaRequest.setCodTipoVehMinTrans(soatTransaction.getCodTipoVehMinTrans());
						expedirPolizaRequest.setCorreo(user.getEmail());
						expedirPolizaRequest.setDireccion(user.getDireccion());
						expedirPolizaRequest.setFechaExpedicion(soatTransaction.getFechaExpedicion());
						expedirPolizaRequest.setFechaFinVigencia(soatTransaction.getFechaFinVigencia());
						expedirPolizaRequest.setFechaInicioVigencia(soatTransaction.getFechaInicioVigencia());
						expedirPolizaRequest.setLinea(soatTransaction.getLinea());
						expedirPolizaRequest.setModelo(soatTransaction.getModelo());
						expedirPolizaRequest.setNoChasis(soatTransaction.getNoChasis());
						expedirPolizaRequest.setNombre(user.getNombre());
						expedirPolizaRequest.setNoDoc(user.getCedula());
						expedirPolizaRequest.setNoMotor(soatTransaction.getNoMotor());
						expedirPolizaRequest.setNoVin(soatTransaction.getNoVin());
						expedirPolizaRequest.setPlaca(soatTransaction.getPlaca());
						expedirPolizaRequest.setTarifa(soatTransaction.getTarifa());
						expedirPolizaRequest.setTelefono(user.getTelefono());
						expedirPolizaRequest.setValorContribucion(soatTransaction.getValorContribucion());
						expedirPolizaRequest.setValorPrima(soatTransaction.getValorPrima());
						expedirPolizaRequest.setValorTasaRUNT(soatTransaction.getValorTasaRunt());
						expedirPolizaRequest.setValorTotalPagar(soatTransaction.getValorTotalPagar());
						expedirPolizaRequest.setValorTotalPoliza(soatTransaction.getValorTotalPoliza());
						expedirPolizaRequest.setCodDestino(soatTransaction.getCodDestino());
						expedirPolizaRequest.setCodDpto(Integer.parseInt(user.getDepartamento()));
						expedirPolizaRequest.setCodMunicipio(Integer.parseInt(user.getProvincia()));
						LOG.info("Solicitando la poliza a Soat {}", expedirPolizaRequest);
						response = client.expedirPoliza(expedirPolizaRequest);
						LOG.info("Respuesta de soat {}", response);
						if (response.getCodigo() != -1) {
							soatTransaction.setStatus(1);
							LOG.info("Actualizando el status de la placa en la BD, {}",soatTransaction.getPlaca());
							soatRepo.save(soatTransaction);
							bdUtils.ingresarBitacoraPuntoRed("Expedir Póliza SOAT", "Póliza Expedida", request.getIdUsuario(), "USUARIO", "SOAT Service", request.getIdPais(), request.getIdApp(), request.getIdioma(), 230L, 2300L);
							Map<String, String> params = new HashMap<String, String>();
							params.put("<placa>", request.getPlaca());
							params.put("<nombre>", user.getNombre()+" "+user.getApellido());
							pushService.sendPush(params, request.getIdioma(), request.getIdPais(), request.getIdApp().intValue(), request.getIdUsuario(), "soat_exito");
						}else {
							LOG.error("Poliza rechazada");
							
						}						
					}else {
						bdUtils.ingresarBitacoraPuntoRed("Expedir Póliza SOAT", "Transaccion Denegada", request.getIdUsuario(), "USUARIO", "SOAT Service", request.getIdPais(), request.getIdApp(), request.getIdioma(), 230L, 2300L);
						if (paymentResponse.getCode() == -3100) {
							throw new Exception(paymentResponse.getMessage());
						}else {
							throw new Exception("Transaccion Denegada por Payment Bakery");
						}
					}
				}else {
					LOG.error("La placa ya se encuentra en un proceso de poliza");
					throw new Exception("La placa ya se encuentra en un proceso de poliza");
				}
			}
		} catch (Exception e) {
			LOG.error("No se puede generar la poliza {}", e.getMessage());
			bdUtils.ingresarBitacoraPuntoRed("Error al expedir una poliza de SOAT", e.getMessage(), request.getIdUsuario(), "USUARIO", "SOAT Service", request.getIdPais(), request.getIdApp(), request.getIdioma(), 230L, 2300L);
			throw new Exception(e.getMessage());
		}
		return response;
	}
	
	private ResponseMc validarPlaca(ConsultarInfoVehiculoResponse vehiculo) throws Exception {
		ResponseMc response = new ResponseMc();
		LOG.info("Iniciando la validacion del vehiculo");
		if (vehiculo.getEstadoDelVehiculo() != null) {
			if (!vehiculo.getEstadoDelVehiculo().equals("ACTIVO")) {
				LOG.error("Vehiculo inactivo");
				response.setCodigo(-1);
				response.setMensaje("Vehiculo inactivo");
			}else if(bdUtils.validaOrganismo(vehiculo.getOrganismoTransito()) > 0) {
				LOG.error("Organismo de tránsito no válido para póliza");
				response.setCodigo(-1);
				response.setMensaje("Organismo de tránsito no válido para póliza");
			}else {
				response.setCodigo(0);
				response.setMensaje("El vehiculo es candidato para expedir la poliza");
			}
		}else {
			LOG.error("Datos insuficientes por parte de SOAT para calcular la poliza");
			response.setCodigo(-1);
			response.setMensaje("Datos insuficientes por parte de SOAT para calcular la poliza");
		}
		
		return response;
	}
	
	private ConsultarInfoVehiculoResponse consultarPlacaSoat(String placa) throws Exception {
		ConsultaPlacaRequest vehiculoRequest = null;
		ConsultarInfoVehiculoResponse vehiculoResponse = null;
		ResponseMc validaPlaca = null;
		try {
			vehiculoRequest = new ConsultaPlacaRequest(placa);
			LOG.info("Consultando la info de la placa {} en SOAT", placa);
			vehiculoResponse = client.consultaInfoVehiculo(vehiculoRequest);
			LOG.info("Respuesta de SOAT: {}", vehiculoResponse);
			validaPlaca = validarPlaca(vehiculoResponse);
			if (validaPlaca.getCodigo() == 0) {
				LOG.info("El vehiculo es candidato para expedir la poliza");
				return vehiculoResponse;
			}else {
				throw new Exception(validaPlaca.getMensaje());
			}
		} catch (Exception e) {
			LOG.error("La placa no es candidata para una poliza");
			throw new Exception(e.getMessage());
		}
	}
	
	private CalculaPolizaResponse calcularPolizaSoat(Double capacidad, Integer cilindraje, Integer clase, Integer modelo, Integer pasajeros, String placa) throws Exception {
		ConsultaPolizaRequest polizaRequest = null;
		CalculaPolizaResponse polizaResponse = null;
		try {
			LocalDate date = LocalDate.now();
			LocalDate limit = date.plusDays(30);
			String escape = Pattern.quote("/");
			LOG.info("Generando el request para calcular la poliza");
			polizaRequest = new ConsultaPolizaRequest();
			polizaRequest.setCapacidad(capacidad);
			polizaRequest.setCilindraje(cilindraje);
			polizaRequest.setClase(clase);
			polizaRequest.setModelo(modelo);
			polizaRequest.setPasajeros(pasajeros);
			polizaRequest.setPlaca(placa);
			LOG.info("Calculando la poliza en SOAT con los datos {}",polizaRequest);
			polizaResponse = client.calcularPoliza(polizaRequest);
			LOG.info("Respuesta del calculo de poliza: {}", polizaResponse);
			if (polizaResponse.getResultadoExito().equals("true")) {
				String[] fecha = polizaResponse.getFechaInicioVigencia().split(escape);
				LocalDate ldFecha = LocalDate.of(Integer.parseInt(fecha[2]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[0]));
				LOG.info("La poliza ha sido calculada con exito");
				if (ldFecha.isBefore(limit)) {
					return polizaResponse;
				}else {
					throw new Exception("La fecha de inicio de la vigencia debe ser menor a 30 días");
				}
			}else {
				throw new Exception("Poliza rechazada por SOAT");
			}
		} catch (Exception e) {
			LOG.error("Error al calcular la poliza: {}",e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
}
