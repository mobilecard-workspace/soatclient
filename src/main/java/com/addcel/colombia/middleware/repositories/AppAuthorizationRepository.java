package com.addcel.colombia.middleware.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.addcel.colombia.middleware.entities.model.AppAuthorization;

@Repository
public interface AppAuthorizationRepository extends JpaRepository<AppAuthorization, Integer>{

	AppAuthorization findByIdApplicationAndActivo(String idApplication, String activo);
}
