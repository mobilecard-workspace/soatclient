package com.addcel.colombia.middleware.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.addcel.colombia.middleware.entities.model.TagViaRapida;

@Repository
public interface TagViaRapidaRepository extends JpaRepository<TagViaRapida, Long> {

	List<TagViaRapida> findByUsuario_Id(Long idUsuario);
	
	@Query("SELECT t FROM TagViaRapida t WHERE t.idViaTag = ?1")
	Optional<TagViaRapida> findById(Long idTag);
	
	TagViaRapida findByidViaTagAndUsuario_Id(Long idTag, Long idUsuario);
	
}
