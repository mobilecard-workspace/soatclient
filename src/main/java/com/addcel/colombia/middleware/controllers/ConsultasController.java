package com.addcel.colombia.middleware.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.colombia.middleware.entities.dto.ConsultaSaldoResponse;
import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.entities.dto.response.EstadoTransaccionResponse;
import com.addcel.colombia.middleware.service.ConsultasService;
import com.addcel.colombia.middleware.utils.AppUtils;
import com.addcel.colombia.middleware.utils.BdUtils;

@RestController
public class ConsultasController {

	@Autowired
	ConsultasService service;
	@Autowired 
	BdUtils bdUtils;
	
	private final static String INVALIDCOUNTRY = "Servicio valido solamente para Colombia";
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseMc> handleException(Exception e) {
        e.printStackTrace();
        int code = -1;
        String message = e.getMessage();
        return new ResponseEntity<>(new ResponseMc(code, message), HttpStatus.OK);
    }
	
	@GetMapping(value = AppUtils.CONSULTAR_SALDO)
	public ResponseEntity<ConsultaSaldoResponse> consultaSaldoEstablecimiento(@RequestParam long idUsuario, @PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma) throws Exception{
		if(bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<ConsultaSaldoResponse>(service.consultarSaldoPuntoVenta(idUsuario, idApp, idPais, idioma),HttpStatus.OK);
	}
	
	@GetMapping(value = AppUtils.ESTATUS_TRANSACCION)
	public ResponseEntity<EstadoTransaccionResponse> consultaEstadoTransaccion(@RequestParam long idUsuario, @RequestParam String trace, @PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma) throws Exception{
		if(bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<EstadoTransaccionResponse>(service.consultaStatusTransaccion(idUsuario, trace, idApp, idPais, idioma), HttpStatus.OK);
	}
}
