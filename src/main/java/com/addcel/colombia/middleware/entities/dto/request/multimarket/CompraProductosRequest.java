package com.addcel.colombia.middleware.entities.dto.request.multimarket;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompraProductosRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idUsuario;
	private Integer idPais;
	private Long idApp;
	private String idioma;
	private Long idTarjeta;
	private Long idProducto;
	private Double amount;
	private Double comision;
}
