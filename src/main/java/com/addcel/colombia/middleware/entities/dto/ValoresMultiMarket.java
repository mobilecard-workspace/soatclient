package com.addcel.colombia.middleware.entities.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValoresMultiMarket implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer productId;
	private String valor;

}
