package com.addcel.colombia.middleware.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.addcel.colombia.middleware.entities.dto.AppUser;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private AppUser appUser;
	@Autowired
	private AuthenticationEntryPoint authEntry;
	private static final String DEFAULT_ROLE = "USER";

	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http.csrf().disable()
			.authorizeRequests().anyRequest().authenticated()
			.and()
			.httpBasic()
			.authenticationEntryPoint(authEntry);
	}
	
	@Autowired
	protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
		auth.inMemoryAuthentication()
			.withUser(appUser.getUsername())
			.password("{noop}"+appUser.getPassword())
			.roles(DEFAULT_ROLE);
	}
}
