package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="notificaciones_push")
public class PushNotification implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id_notificacion_push")
	private Long id;
	@Column(name = "modulo")
	private String modulo;
	@Column(name = "titulo")
	private String titulo;
	@Column(name = "mensaje")
	private String mensaje;
	@Column(name = "idioma")
	private String idioma;
}
